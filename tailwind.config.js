module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        'tablet': '960px',
        'desktop': '1238px'
      },
      width: {
        '60': '60px'
      },
      height: {
        '60': '60px',
        '75': '75px',
        '340px': '340px'
      },
      borderRadius: {
        '50px': "50px",
        '40px': "40px",
        '45px': "45px",
        '25px': '25px',
        '20px': '20px',
        'inherit': 'inherit'
      },
      padding: {
        '40px': '40px',
      },
      backgrounds: {
        'black-50': '#333333',
        'black-100': '#212121',
        'black-200': '#1A1A1A',
        'black-300': '#0D0D0D',
        'black-400': '#333333'
      },
      colors: {
        'white': '#FFFFFF',
        'gray-50': '#F9FAFB',
        'gray-100': '#F3F4F6',
        'gray-200': '#E5E7EB',
        'gray-300': '#D1D5DB',
        'gray-400': '#9CA3AF',
        'raspberry-50': '#E97296',
        'raspberry-100': '#E66088',
        'raspberry-200': '#E34F7B',
        'raspberry-300': '#E13D6E',
        'raspberry-400': '#DC225A',
        'orange-50': '#FDC05D',
        'orange-100': '#FDB849',
        'orange-200': '#FDB035',
        'orange-300': '#FDA821',
        'orange-400': '#FD9C02',
        'green-50': '#78E39F',
        'green-100': '#67E093',
        'green-200': '#56DC87',
        'green-300': '#45D97B',
        'green-400': '#2BD268',
        'blue-50': '#859DFF',
        'blue-100': '#708DFF',
        'blue-200': '#5C7CFF',
        'blue-300': '#476CFF',
        'blue-400': '#234EFF',
        'crayola-50': '#FEA486',
        'crayola-100': '#FE9572',
        'crayola-200': '#FD855D',
        'crayola-300': '#FD7649',
        'crayola-400': '#FD6835',
        'black-50': '#333333',
        'black-100': '#212121',
        'black-200': '#1A1A1A',
        'black-300': '#0D0D0D',
        'black-400': '#333333'
      },
      borderColor: {
        'black-50': '#333333',
        'black-100': '#212121',
        'black-200': '#1A1A1A',
        'black-300': '#0D0D0D',
        'black-400': '#333333'
      },
      fontFamily: {
        'dm': ['"DM Sans"', 'sans-serif']
      },
      fontSize: {
        'xs': ['.75rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        'sm': ['.875rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        'base': ['1rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        'lg': ['1.125rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        'xl': ['1.25rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        '2xl': ['1.5rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        '3xl': ['1.875rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        '4xl': ['2.25rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        '5xl': ['3rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
        '6xl': ['4rem', { lineHeight: '18px', letterSpacing: '-0.03em' }],
      }
    },
  }, 
  plugins: [],
}