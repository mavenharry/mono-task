import React from "react";

// Components
import Header from "../../Components/Header";
import Sidebar from "../../Components/Sidebar";
import Playlist from "../../Components/Playlist";
import MostListenedTracks from "../../Components/MostListenedTracks";
import TracksOfTheWeek from "../../Components/TracksOfTheWeek";
import FriendsActivity from "../../Components/FriendsActivity";
import Player from "../../Components/Player";

export default function Home() {
  return (
    <div className="bg-black w-full h-full p-5 pb-32 overflow-x-hidden">
        <Header/>
        <div className="flex justify-between items-start">
          <Sidebar/>
          <div className="block w-full">
            <Playlist/>
            <div className="flex">
              <MostListenedTracks/>
              <TracksOfTheWeek/>
              <FriendsActivity/>
            </div>
          </div>
          <Player/>
        </div>
    </div>
  )
}