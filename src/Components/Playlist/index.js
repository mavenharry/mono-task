import React from "react";
import "./index.css";

export default function Player() {
  return (
    <div className="h-auto mt-7 ml-7 bg-transparent relative">
          <div className="text-xl font-dm font-normal text-white">
            <h4>Playlist for you</h4>
          </div>
          <div className="flex justify-start mt-7 items-center">

            <div className="h-340px mr-5 transform transition duration-500 hover:scale-105 cursor-pointer last-of-type:mr-0 rounded-40px w-1/4 relative">
              <div className="font-normal text-base font-dm text-white absolute top-6 left-6">
                <h4>39 Tracks</h4>
              </div>
              <div className="h-full w-full rounded-inherit">
                <img className="h-full object-cover object-top w-full rounded-inherit" alt="Playlist image" src={require('../../Assets/Images/M2.jpg')}/>
              </div>
              <div className="z-40 absolute bottom-6 left-6 flex justify-center items-center">
                <div className="group mr-2 cursor-pointer">
                  <svg className="group-hover:hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M24 48C30.3652 48 36.4697 45.4714 40.9706 40.9706C45.4714 36.4697 48 30.3652 48 24C48 17.6348 45.4714 11.5303 40.9706 7.02944C36.4697 2.52856 30.3652 0 24 0C17.6348 0 11.5303 2.52856 7.02944 7.02944C2.52856 11.5303 0 17.6348 0 24C0 30.3652 2.52856 36.4697 7.02944 40.9706C11.5303 45.4714 17.6348 48 24 48ZM22.665 15.504C22.2132 15.2026 21.688 15.0294 21.1455 15.0031C20.6031 14.9767 20.0636 15.0981 19.5847 15.3544C19.1058 15.6106 18.7055 15.992 18.4264 16.458C18.1473 16.9239 17.9999 17.4569 18 18V30C17.9999 30.5431 18.1473 31.0761 18.4264 31.542C18.7055 32.008 19.1058 32.3894 19.5847 32.6456C20.0636 32.9019 20.6031 33.0233 21.1455 32.9969C21.688 32.9706 22.2132 32.7974 22.665 32.496L31.665 26.496C32.0759 26.222 32.4128 25.8509 32.6458 25.4155C32.8788 24.98 33.0007 24.4938 33.0007 24C33.0007 23.5062 32.8788 23.02 32.6458 22.5845C32.4128 22.1491 32.0759 21.778 31.665 21.504L22.665 15.504Z" fill="white"/>
                  </svg>
                  <svg className="group-hover:block hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M48 24C48 30.3652 45.4714 36.4697 40.9706 40.9706C36.4697 45.4714 30.3652 48 24 48C17.6348 48 11.5303 45.4714 7.02944 40.9706C2.52856 36.4697 0 30.3652 0 24C0 17.6348 2.52856 11.5303 7.02944 7.02944C11.5303 2.52856 17.6348 0 24 0C30.3652 0 36.4697 2.52856 40.9706 7.02944C45.4714 11.5303 48 17.6348 48 24V24ZM15 18C15 17.2044 15.3161 16.4413 15.8787 15.8787C16.4413 15.3161 17.2044 15 18 15C18.7956 15 19.5587 15.3161 20.1213 15.8787C20.6839 16.4413 21 17.2044 21 18V30C21 30.7956 20.6839 31.5587 20.1213 32.1213C19.5587 32.6839 18.7956 33 18 33C17.2044 33 16.4413 32.6839 15.8787 32.1213C15.3161 31.5587 15 30.7956 15 30V18ZM30 15C29.2044 15 28.4413 15.3161 27.8787 15.8787C27.3161 16.4413 27 17.2044 27 18V30C27 30.7956 27.3161 31.5587 27.8787 32.1213C28.4413 32.6839 29.2044 33 30 33C30.7956 33 31.5587 32.6839 32.1213 32.1213C32.6839 31.5587 33 30.7956 33 30V18C33 17.2044 32.6839 16.4413 32.1213 15.8787C31.5587 15.3161 30.7956 15 30 15Z" fill="white"/>
                  </svg>
                </div>
                <div className="playlist-card-details-text">
                  <div className="font-semibold font-dm text-base text-white"><h4>Chill Mix</h4></div>
                  <div className="font-thin font-dm text-base text-white"><p>Just relax and listen</p></div>
                </div>
              </div>
              <div className="absolute bottom-0 h-full w-full rounded-40px floating-radient-1"></div>
            </div>

            <div className="h-340px mr-5 transform transition duration-500 hover:scale-105 cursor-pointer last-of-type:mr-0 rounded-40px w-1/4 relative">
              <div className="font-normal text-base font-dm text-white absolute top-6 left-6">
                <h4>47 Tracks</h4>
              </div>
              <div className="h-full w-full rounded-inherit">
                <img className="h-full object-cover object-top w-full rounded-inherit" alt="Playlist image" src={require('../../Assets/Images/MA1.jpg')}/>
              </div>
              <div className="z-40 absolute bottom-6 left-6 flex justify-center items-center">
                <div className="group mr-2 cursor-pointer">
                  <svg className="group-hover:hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M24 48C30.3652 48 36.4697 45.4714 40.9706 40.9706C45.4714 36.4697 48 30.3652 48 24C48 17.6348 45.4714 11.5303 40.9706 7.02944C36.4697 2.52856 30.3652 0 24 0C17.6348 0 11.5303 2.52856 7.02944 7.02944C2.52856 11.5303 0 17.6348 0 24C0 30.3652 2.52856 36.4697 7.02944 40.9706C11.5303 45.4714 17.6348 48 24 48ZM22.665 15.504C22.2132 15.2026 21.688 15.0294 21.1455 15.0031C20.6031 14.9767 20.0636 15.0981 19.5847 15.3544C19.1058 15.6106 18.7055 15.992 18.4264 16.458C18.1473 16.9239 17.9999 17.4569 18 18V30C17.9999 30.5431 18.1473 31.0761 18.4264 31.542C18.7055 32.008 19.1058 32.3894 19.5847 32.6456C20.0636 32.9019 20.6031 33.0233 21.1455 32.9969C21.688 32.9706 22.2132 32.7974 22.665 32.496L31.665 26.496C32.0759 26.222 32.4128 25.8509 32.6458 25.4155C32.8788 24.98 33.0007 24.4938 33.0007 24C33.0007 23.5062 32.8788 23.02 32.6458 22.5845C32.4128 22.1491 32.0759 21.778 31.665 21.504L22.665 15.504Z" fill="white"/>
                  </svg>
                  <svg className="group-hover:block hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M48 24C48 30.3652 45.4714 36.4697 40.9706 40.9706C36.4697 45.4714 30.3652 48 24 48C17.6348 48 11.5303 45.4714 7.02944 40.9706C2.52856 36.4697 0 30.3652 0 24C0 17.6348 2.52856 11.5303 7.02944 7.02944C11.5303 2.52856 17.6348 0 24 0C30.3652 0 36.4697 2.52856 40.9706 7.02944C45.4714 11.5303 48 17.6348 48 24V24ZM15 18C15 17.2044 15.3161 16.4413 15.8787 15.8787C16.4413 15.3161 17.2044 15 18 15C18.7956 15 19.5587 15.3161 20.1213 15.8787C20.6839 16.4413 21 17.2044 21 18V30C21 30.7956 20.6839 31.5587 20.1213 32.1213C19.5587 32.6839 18.7956 33 18 33C17.2044 33 16.4413 32.6839 15.8787 32.1213C15.3161 31.5587 15 30.7956 15 30V18ZM30 15C29.2044 15 28.4413 15.3161 27.8787 15.8787C27.3161 16.4413 27 17.2044 27 18V30C27 30.7956 27.3161 31.5587 27.8787 32.1213C28.4413 32.6839 29.2044 33 30 33C30.7956 33 31.5587 32.6839 32.1213 32.1213C32.6839 31.5587 33 30.7956 33 30V18C33 17.2044 32.6839 16.4413 32.1213 15.8787C31.5587 15.3161 30.7956 15 30 15Z" fill="white"/>
                  </svg>
                </div>
                <div className="playlist-card-details-text">
                  <div className="font-semibold font-dm text-base text-white"><h4>Gamer Mix</h4></div>
                  <div className="font-thin font-dm text-base text-white"><p>Listen while you play</p></div>
                </div>
              </div>
              <div className="absolute bottom-0 h-full w-full rounded-40px floating-radient-2"></div>
            </div>

            <div className="h-340px mr-5 transform transition duration-500 hover:scale-105 cursor-pointer last-of-type:mr-0 rounded-40px w-1/4 relative">
              <div className="font-normal text-base font-dm text-white absolute top-6 left-6">
                <h4>26 Tracks</h4>
              </div>
              <div className="h-full w-full rounded-inherit">
                <img className="h-full object-cover object-top w-full rounded-inherit" alt="Playlist image" src={require('../../Assets/Images/M1.jpg')}/>
              </div>
              <div className="z-40 absolute bottom-6 left-6 flex justify-center items-center">
                <div className="group mr-2 cursor-pointer">
                  <svg className="group-hover:hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M24 48C30.3652 48 36.4697 45.4714 40.9706 40.9706C45.4714 36.4697 48 30.3652 48 24C48 17.6348 45.4714 11.5303 40.9706 7.02944C36.4697 2.52856 30.3652 0 24 0C17.6348 0 11.5303 2.52856 7.02944 7.02944C2.52856 11.5303 0 17.6348 0 24C0 30.3652 2.52856 36.4697 7.02944 40.9706C11.5303 45.4714 17.6348 48 24 48ZM22.665 15.504C22.2132 15.2026 21.688 15.0294 21.1455 15.0031C20.6031 14.9767 20.0636 15.0981 19.5847 15.3544C19.1058 15.6106 18.7055 15.992 18.4264 16.458C18.1473 16.9239 17.9999 17.4569 18 18V30C17.9999 30.5431 18.1473 31.0761 18.4264 31.542C18.7055 32.008 19.1058 32.3894 19.5847 32.6456C20.0636 32.9019 20.6031 33.0233 21.1455 32.9969C21.688 32.9706 22.2132 32.7974 22.665 32.496L31.665 26.496C32.0759 26.222 32.4128 25.8509 32.6458 25.4155C32.8788 24.98 33.0007 24.4938 33.0007 24C33.0007 23.5062 32.8788 23.02 32.6458 22.5845C32.4128 22.1491 32.0759 21.778 31.665 21.504L22.665 15.504Z" fill="white"/>
                  </svg>
                  <svg className="group-hover:block hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M48 24C48 30.3652 45.4714 36.4697 40.9706 40.9706C36.4697 45.4714 30.3652 48 24 48C17.6348 48 11.5303 45.4714 7.02944 40.9706C2.52856 36.4697 0 30.3652 0 24C0 17.6348 2.52856 11.5303 7.02944 7.02944C11.5303 2.52856 17.6348 0 24 0C30.3652 0 36.4697 2.52856 40.9706 7.02944C45.4714 11.5303 48 17.6348 48 24V24ZM15 18C15 17.2044 15.3161 16.4413 15.8787 15.8787C16.4413 15.3161 17.2044 15 18 15C18.7956 15 19.5587 15.3161 20.1213 15.8787C20.6839 16.4413 21 17.2044 21 18V30C21 30.7956 20.6839 31.5587 20.1213 32.1213C19.5587 32.6839 18.7956 33 18 33C17.2044 33 16.4413 32.6839 15.8787 32.1213C15.3161 31.5587 15 30.7956 15 30V18ZM30 15C29.2044 15 28.4413 15.3161 27.8787 15.8787C27.3161 16.4413 27 17.2044 27 18V30C27 30.7956 27.3161 31.5587 27.8787 32.1213C28.4413 32.6839 29.2044 33 30 33C30.7956 33 31.5587 32.6839 32.1213 32.1213C32.6839 31.5587 33 30.7956 33 30V18C33 17.2044 32.6839 16.4413 32.1213 15.8787C31.5587 15.3161 30.7956 15 30 15Z" fill="white"/>
                  </svg>
                </div>
                <div className="playlist-card-details-text">
                  <div className="font-semibold font-dm text-base text-white"><h4>Future Mix</h4></div>
                  <div className="font-thin font-dm text-base text-white"><p>Of your favorites</p></div>
                </div>
              </div>
              <div className="absolute bottom-0 h-full w-full rounded-40px floating-radient-3"></div>
            </div>

            <div className="h-340px mr-5 transform transition duration-500 hover:scale-105 cursor-pointer last-of-type:mr-0 rounded-40px w-1/4 relative">
              <div className="font-normal text-base font-dm text-white absolute top-6 left-6">
                <h4>39 Tracks</h4>
              </div>
              <div className="h-full w-full rounded-inherit">
                <img className="h-full object-cover object-top w-full rounded-inherit" alt="Playlist image" src={require('../../Assets/Images/H1.jpg')}/>
              </div>
              <div className="z-40 absolute bottom-6 left-6 flex justify-center items-center">
                <div className="group mr-2 cursor-pointer">
                  <svg className="group-hover:hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M24 48C30.3652 48 36.4697 45.4714 40.9706 40.9706C45.4714 36.4697 48 30.3652 48 24C48 17.6348 45.4714 11.5303 40.9706 7.02944C36.4697 2.52856 30.3652 0 24 0C17.6348 0 11.5303 2.52856 7.02944 7.02944C2.52856 11.5303 0 17.6348 0 24C0 30.3652 2.52856 36.4697 7.02944 40.9706C11.5303 45.4714 17.6348 48 24 48ZM22.665 15.504C22.2132 15.2026 21.688 15.0294 21.1455 15.0031C20.6031 14.9767 20.0636 15.0981 19.5847 15.3544C19.1058 15.6106 18.7055 15.992 18.4264 16.458C18.1473 16.9239 17.9999 17.4569 18 18V30C17.9999 30.5431 18.1473 31.0761 18.4264 31.542C18.7055 32.008 19.1058 32.3894 19.5847 32.6456C20.0636 32.9019 20.6031 33.0233 21.1455 32.9969C21.688 32.9706 22.2132 32.7974 22.665 32.496L31.665 26.496C32.0759 26.222 32.4128 25.8509 32.6458 25.4155C32.8788 24.98 33.0007 24.4938 33.0007 24C33.0007 23.5062 32.8788 23.02 32.6458 22.5845C32.4128 22.1491 32.0759 21.778 31.665 21.504L22.665 15.504Z" fill="white"/>
                  </svg>
                  <svg className="group-hover:block hidden w-11 h-11" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M48 24C48 30.3652 45.4714 36.4697 40.9706 40.9706C36.4697 45.4714 30.3652 48 24 48C17.6348 48 11.5303 45.4714 7.02944 40.9706C2.52856 36.4697 0 30.3652 0 24C0 17.6348 2.52856 11.5303 7.02944 7.02944C11.5303 2.52856 17.6348 0 24 0C30.3652 0 36.4697 2.52856 40.9706 7.02944C45.4714 11.5303 48 17.6348 48 24V24ZM15 18C15 17.2044 15.3161 16.4413 15.8787 15.8787C16.4413 15.3161 17.2044 15 18 15C18.7956 15 19.5587 15.3161 20.1213 15.8787C20.6839 16.4413 21 17.2044 21 18V30C21 30.7956 20.6839 31.5587 20.1213 32.1213C19.5587 32.6839 18.7956 33 18 33C17.2044 33 16.4413 32.6839 15.8787 32.1213C15.3161 31.5587 15 30.7956 15 30V18ZM30 15C29.2044 15 28.4413 15.3161 27.8787 15.8787C27.3161 16.4413 27 17.2044 27 18V30C27 30.7956 27.3161 31.5587 27.8787 32.1213C28.4413 32.6839 29.2044 33 30 33C30.7956 33 31.5587 32.6839 32.1213 32.1213C32.6839 31.5587 33 30.7956 33 30V18C33 17.2044 32.6839 16.4413 32.1213 15.8787C31.5587 15.3161 30.7956 15 30 15Z" fill="white"/>
                  </svg>
                </div>
                <div className="playlist-card-details-text">
                  <div className="font-semibold font-dm text-base text-white"><h4>Classical Mix</h4></div>
                  <div className="font-thin font-dm text-base text-white"><p>Just relax and listen</p></div>
                </div>
              </div>
              <div className="absolute bottom-0 h-full w-full rounded-40px floating-radient-4"></div>
            </div>

          </div>

          <div className="absolute right-0 top-0 w-1/4 floating-arrow-right">
            <svg width="378" height="441" className="cursor-pointer" viewBox="0 0 378 441" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect width="379" height="441" fill="url(#paint0_linear_81_441)"/>
              <path fill-rule="evenodd" clip-rule="evenodd" d="M313.586 229.914C313.211 229.539 313 229.03 313 228.5C313 227.97 313.211 227.461 313.586 227.086L320.172 220.5L313.586 213.914C313.222 213.537 313.02 213.032 313.025 212.507C313.029 211.983 313.24 211.481 313.61 211.11C313.981 210.74 314.483 210.529 315.007 210.525C315.532 210.52 316.037 210.722 316.414 211.086L324.414 219.086C324.789 219.461 325 219.97 325 220.5C325 221.03 324.789 221.539 324.414 221.914L316.414 229.914C316.039 230.289 315.53 230.5 315 230.5C314.47 230.5 313.961 230.289 313.586 229.914Z" fill="white"/>
            </svg>
          </div>

    </div>
  );
}