import React from "react";
import "./index.css";

export default function Header() {
  return (
    <div className="flex items-center justify-center w-full">
      
      {/* Logo */}
      <div className="flex w-20 items-center justify-center h-16">
          <img className="h-full object-contain w-5/6" alt="Spotify logo" src={require('../../Assets/Images/Logo.png')}/>
      </div>

      {/* Search bar */}
      <div className="h-14 border-black-50 pl-4 pr-5 rounded-full w-full my-0 mx-6 flex border-solid border box-border justify-between items-center">
        <div className="h-5 w-5 mr-3 cursor-pointer">
          <svg className="text-white" viewBox="0 0 24 25" fill="none" className="h-full w-full" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12" cy="12.5" r="10.5" stroke="white" stroke-width="3"/>
          </svg>
        </div>
        <div className="w-full">
          <input className="outline-0 text-white bg-transparent h-14 w-full font-dm text-base font-normal not-italic" type="text" placeholder="Search..." defaultValue="" />
        </div>
        <div className="flex justify-center items-center cursor-pointer">
          <div className="h-5 w-5 mr-3 cursor-pointer mr-4">
            <svg className="text-white" viewBox="0 0 25 19" fill="none" className="h-full w-full" xmlns="http://www.w3.org/2000/svg">
              <path d="M23 5.99985C23.4641 5.99985 23.9092 5.81547 24.2374 5.48728C24.5656 5.15909 24.75 4.71398 24.75 4.24985C24.75 3.78572 24.5656 3.3406 24.2374 3.01241C23.9092 2.68422 23.4641 2.49985 23 2.49985L20.781 2.49985C20.4738 1.96782 20.032 1.52603 19.4999 1.21887C18.9679 0.911707 18.3643 0.75 17.75 0.75C17.1357 0.75 16.5321 0.911707 16.0001 1.21887C15.468 1.52603 15.0262 1.96782 14.719 2.49985L2 2.49985C1.53587 2.49985 1.09075 2.68422 0.762562 3.01241C0.434374 3.3406 0.25 3.78572 0.25 4.24985C0.25 4.71397 0.434374 5.15909 0.762562 5.48728C1.09075 5.81547 1.53587 5.99985 2 5.99985L14.719 5.99985C15.0262 6.53187 15.468 6.97366 16.0001 7.28083C16.5321 7.58799 17.1357 7.74969 17.75 7.74969C18.3643 7.74969 18.9679 7.58799 19.4999 7.28083C20.032 6.97366 20.4738 6.53187 20.781 5.99985L23 5.99985ZM24.75 14.7498C24.75 15.214 24.5656 15.6591 24.2374 15.9873C23.9092 16.3155 23.4641 16.4998 23 16.4998L10.281 16.4998C9.9738 17.0319 9.53197 17.4737 8.99992 17.7808C8.46787 18.088 7.86435 18.2497 7.25 18.2497C6.63565 18.2497 6.03212 18.088 5.50008 17.7808C4.96803 17.4737 4.5262 17.0319 4.219 16.4998L2 16.4998C1.53587 16.4998 1.09075 16.3155 0.762561 15.9873C0.434373 15.6591 0.249999 15.214 0.249999 14.7498C0.249999 14.2857 0.434373 13.8406 0.762561 13.5124C1.09075 13.1842 1.53587 12.9998 2 12.9998L4.219 12.9998C4.5262 12.4678 4.96803 12.026 5.50008 11.7189C6.03213 11.4117 6.63565 11.25 7.25 11.25C7.86435 11.25 8.46787 11.4117 8.99992 11.7189C9.53197 12.026 9.9738 12.4678 10.281 12.9998L23 12.9998C23.4641 12.9998 23.9092 13.1842 24.2374 13.5124C24.5656 13.8406 24.75 14.2857 24.75 14.7498Z" fill="white"/>
            </svg>
          </div>
          <div className="h-5 w-5 mr-5 cursor-pointer text-white pl-0 text-sm">
            <h4>Filters</h4>
          </div>
        </div>
      </div>

      {/* Options */}
      <div className="w-auto justify-center items-center flex border-black-50 bg-black-300 rounded-full h-14 border-solid border box-border">
        <div className="flex justify-center items-center pl-5">
          <div className="h-5 w-5 mr-5 cursor-pointer">
            <svg className="w-full h-full" xmlns="http://www.w3.org/2000/svg" className="h-full w-full" viewBox="0 0 20 20" fill="#ffffff">
              <path fillRule="evenodd" d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clipRule="evenodd" />
            </svg>
          </div>
          <div className="h-5 w-5 mr-5 cursor-pointer">
            <svg className="w-full h-full" xmlns="http://www.w3.org/2000/svg" className="h-full w-full" viewBox="0 0 20 20" fill="#ffffff">
              <path fillRule="evenodd" d="M3 6a3 3 0 013-3h10a1 1 0 01.8 1.6L14.25 8l2.55 3.4A1 1 0 0116 13H6a1 1 0 00-1 1v3a1 1 0 11-2 0V6z" clipRule="evenodd" />
            </svg>
          </div>
          <div className="h-5 w-5 mr-5 cursor-pointer">
            <svg className="w-full h-full" xmlns="http://www.w3.org/2000/svg" className="h-full w-full" viewBox="0 0 20 20" fill="#ffffff">
              <path fillRule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
            </svg>
          </div>
          <div className="h-5 w-5 mr-5 cursor-pointer">
            <svg className="w-full h-full" xmlns="http://www.w3.org/2000/svg" className="h-full w-full" viewBox="0 0 20 20" fill="#ffffff">
              <path d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z" />
            </svg>
          </div>
        </div>
        <div className="rounded-full h-14 w-14 p-1 flex justify-end">
          <img className="h-full w-full cursor-pointer" alt="Avatar" src={require('../../Assets/Images/Profile.png')}/>
        </div>
      </div>
      
    </div>
  );
}