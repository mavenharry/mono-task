import React from "react";
import "./index.css";

export default function MostListenedTracks() {
  return (
    <div className="h-auto w-2/5 mt-7 ml-6 bg-transparent">
          
      <div className="text-xl font-dm font-normal text-white">
          <h4>What you listen to the most</h4>
      </div>

      <div className="flex rounded-20px p-5 justify-start mt-7 border border-solid border-black-50 items-center">
          <div className="h-full w-6/12 rounded-lg cursor-pointer mr-4">
            <img className="h-full object-cover object-top w-full rounded-inherit" alt="Listened track image" src={require('../../Assets/Images/image4.jpg')}/>
          </div>
          <div className="listened-tracks-card-grid-small-group h-full rounded-lg w-6/12 grid gap-4 flex-wrap">
            <div className="h-full rounded-lg cursor-pointer w-full">
              <img className="h-full object-cover object-top w-full rounded-inherit" alt="Listened track image" src={require('../../Assets/Images/image11.jpg')}/>
            </div>
            <div className="h-full rounded-lg cursor-pointer w-full">
              <img className="h-full object-cover object-top w-full rounded-inherit" alt="Listened track image" src={require('../../Assets/Images/image5.jpg')}/>
            </div>
            <div className="h-full rounded-lg cursor-pointer w-full">
              <img className="h-full object-cover object-top w-full rounded-inherit" alt="Listened track image" src={require('../../Assets/Images/image3.jpg')}/>
            </div>
            <div className="h-full rounded-lg cursor-pointer w-full">
              <img className="h-full object-cover object-top w-full rounded-inherit" alt="Listened track image" src={require('../../Assets/Images/image6.jpg')}/>
            </div>  
          </div>
          <div className="listened-tracks-card-label flex justify-end w-1/12 items-center">
            <h4 className="font-medium text-2xl font-dm text-blue-50 not-italic">Lo-Fi Music</h4>
          </div>
      </div>
        
    </div>
  );
}