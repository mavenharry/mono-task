import React from "react";
import "./index.css";

export default function FriendsActivity() {
  return (
    <div className="h-auto w-1/5 mt-7 min-h-max ml-6 bg-transparent min-h-full">
          
      <div className="text-xl font-dm font-normal text-white">
          <h4>Friends Activity</h4>
      </div>

      <div className="rounded-20px p-5 justify-center mt-7 border border-solid border-black-50 items-center p-3">

          <div className="ml-1 mt-1 mb-4 text-lg font-dm font-normal text-white">
              <h4>Mi Friends</h4>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-10 h-10 bg-gray-100 rounded-full">
                <img className="rounded-full" alt="Listened track image" src={require('../../Assets/Images/friend1.png')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-0 text-sm font-semi-bold text-white font-dm">
                  <h4>Kathryn Murphy</h4>
                </div>
                <div className="flex justify-start items-center text-xs font-light text-gray-400 font-dm">
                  <svg className="w-2.5 h-2.5" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="6.5" cy="6.5" r="6.5" fill="#2BD268"/>
                  </svg>
                  <h4 className="pl-1.5">Listening: Gavin Mikhail</h4>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-10 h-10 bg-gray-100 rounded-full">
                <img className="rounded-full" alt="Listened track image" src={require('../../Assets/Images/friend2.png')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-0 text-sm font-semi-bold text-white font-dm">
                  <h4>Georgina Michael</h4>
                </div>
                <div className="flex justify-start items-center text-xs font-light text-gray-400 font-dm">
                  <svg className="w-2.5 h-2.5 fill-orange-300" viewBox="0 0 13 13" fill="#2BD268" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="6.5" cy="6.5" r="6.5"/>
                  </svg>
                  <h4 className="pl-1.5">Absent</h4>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-10 h-10 bg-gray-100 rounded-full">
                <img className="rounded-full" alt="Listened track image" src={require('../../Assets/Images/friend4.png')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-0 text-sm font-semi-bold text-white font-dm">
                  <h4>Franklin Amachree</h4>
                </div>
                <div className="flex justify-start items-center text-xs font-light text-gray-400 font-dm">
                  <svg className="w-2.5 h-2.5" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="6.5" cy="6.5" r="6.5" fill="#2BD268"/>
                  </svg>
                  <h4 className="pl-1.5">Listening: Dawel Johnson</h4>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-10 h-10 bg-gray-300 rounded-full">
                <img className="rounded-full" alt="Listened track image" src={require('../../Assets/Images/friend3.png')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-0 text-sm font-semi-bold text-white font-dm">
                  <h4>Maven Harry</h4>
                </div>
                <div className="flex justify-start items-center text-xs font-light text-gray-400 font-dm">
                  <svg className="w-2.5 h-2.5 fill-gray-400" viewBox="0 0 13 13" fill="#2BD268" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="6.5" cy="6.5" r="6.5"/>
                  </svg>
                  <h4 className="pl-1.5">Offline</h4>
                </div>
              </div>
            </div>
          </div>

          <div className="w-full h-9 mb-2">
            <button className="rounded text-white text-sm font-dm w-full h-full">View all</button>
          </div>

      </div>
        
    </div>
  );
}