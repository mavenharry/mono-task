import React from "react";
import "./index.css";

export default function TracksOfTheWeek() {
  return (
    <div className="h-auto w-2/5 mt-7 ml-6 bg-transparent">
          
      <div className="text-xl font-dm font-normal text-white">
          <h4>Tracks of the week</h4>
      </div>

      <div className="rounded-20px p-5 justify-center mt-7 border border-solid border-black-50 items-center p-7">

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-12 h-12">
                <img className="rounded" alt="Listened track image" src={require('../../Assets/Images/image11.jpg')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-1 text-sm font-semi-bold text-white font-dm">
                  <h4>Setting Up Our Beds In Minecraft</h4>
                </div>
                <div className="text-sm text-gray-400 font-dm">
                  <h4>Sleepy Fish</h4>
                </div>
              </div>
            </div>
            <div className="flex justify-center items-center">
              <div className="mr-4 flex justify-center items-center">
                <div className="mr-2 w-4.5 h-3.5">
                  <svg className="w-full h-full" viewBox="0 0 18 16" fill="none"  xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.625 9.125H5.0625C3.81973 9.125 2.8125 10.134 2.8125 11.3792V13.6208C2.8125 14.8657 3.81973 15.875 5.0625 15.875H5.625C6.24621 15.875 6.75 15.3702 6.75 14.7479V10.2521C6.75 9.62949 6.24621 9.125 5.625 9.125ZM12.9375 9.125H12.375C11.7538 9.125 11.25 9.62949 11.25 10.2521V14.7479C11.25 15.3702 11.7538 15.875 12.375 15.875H12.9375C14.1803 15.875 15.1875 14.8657 15.1875 13.6208V11.3792C15.1875 10.1343 14.1803 9.125 12.9375 9.125ZM9 0.125C3.96949 0.125 0.160664 4.31316 0 9.125V13.0625C0 13.3733 0.251719 13.625 0.5625 13.625H1.125C1.43578 13.625 1.6875 13.3733 1.6875 13.0625V9.125C1.6875 5.09363 4.96863 1.81953 9 1.81883C13.0314 1.81953 16.3125 5.09363 16.3125 9.125V13.0625C16.3125 13.3733 16.5642 13.625 16.875 13.625H17.4375C17.7483 13.625 18 13.3733 18 13.0625V9.125C17.8393 4.31316 14.0305 0.125 9 0.125Z" fill="white"/>
                  </svg>
                </div>
                <div className="text-white text-sm">
                  <p>311k</p>
                </div>
              </div>
              <div className="border-2 border-solid border-black-50 rounded-full w-24 h-12 flex justify-between items-center">
                <div className="w-1/2 h-full flex justify-center items-center">
                  <svg className="w-4 h-4" viewBox="0 0 21 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.08991 1.96501C3.02755 1.02765 4.29909 0.501077 5.62491 0.501077C6.95073 0.501077 8.22227 1.02765 9.15991 1.96501L10.6249 3.42876L12.0899 1.96501C12.5511 1.48746 13.1029 1.10655 13.7129 0.844502C14.3229 0.582457 14.979 0.444526 15.6429 0.438757C16.3068 0.432988 16.9652 0.559496 17.5797 0.810901C18.1942 1.06231 18.7524 1.43357 19.2219 1.90303C19.6913 2.3725 20.0626 2.93076 20.314 3.54524C20.5654 4.15972 20.6919 4.81812 20.6862 5.48202C20.6804 6.14591 20.5425 6.80201 20.2804 7.41203C20.0184 8.02205 19.6375 8.57378 19.1599 9.03501L10.6249 17.5713L2.08991 9.03501C1.15255 8.09737 0.625977 6.82583 0.625977 5.50001C0.625977 4.17419 1.15255 2.90265 2.08991 1.96501Z" fill="#2BD268"/>
                  </svg>
                </div>
                <div className="cursor-pointer hover:border-green-400 border-2 border-solid flex justify-center items-center border-white rounded-full w-1/2 h-full group">
                  <svg className="w-4 h-4 group-hover:fill-green-400 group-hover:stroke-green-400" width="20" height="24" viewBox="0 0 20 24" fill="white" stroke="white" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.57197 2.49048L16.829 9.33064C17.2684 9.62364 17.6287 10.0238 17.8779 10.4894C18.1271 10.9551 18.2574 11.4751 18.2574 12.0032C18.2574 12.5313 18.1271 13.0513 17.8779 13.5169C17.6287 13.9826 17.2684 14.3795 16.829 14.6725L6.57197 21.5127C6.0888 21.8351 5.52717 22.0202 4.947 22.0484C4.36684 22.0766 3.78991 21.9467 3.27777 21.6727C2.76562 21.3987 2.33747 20.9908 2.039 20.4925C1.74053 19.9942 1.58293 19.4242 1.58302 18.8434V5.16622C1.58176 4.58483 1.73852 4.01403 2.03652 3.51482C2.33453 3.01561 2.76258 2.60676 3.27493 2.33197C3.78729 2.05718 4.36468 1.92677 4.9454 1.95469C5.52612 1.98261 6.08834 2.1678 6.57197 2.49048Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-12 h-12">
                <img className="rounded" alt="Listened track image" src={require('../../Assets/Images/image1.jpg')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-1 text-sm font-semi-bold text-white font-dm">
                  <h4>Kimi Ja Nakya Dame Mitai</h4>
                </div>
                <div className="text-sm text-gray-400 font-dm">
                  <h4>Masayoshi Oishi</h4>
                </div>
              </div>
            </div>
            <div className="flex justify-center items-center">
              <div className="mr-4 flex justify-center items-center">
                <div className="mr-2 w-4.5 h-3.5">
                  <svg className="w-full h-full" viewBox="0 0 18 16" fill="none"  xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.625 9.125H5.0625C3.81973 9.125 2.8125 10.134 2.8125 11.3792V13.6208C2.8125 14.8657 3.81973 15.875 5.0625 15.875H5.625C6.24621 15.875 6.75 15.3702 6.75 14.7479V10.2521C6.75 9.62949 6.24621 9.125 5.625 9.125ZM12.9375 9.125H12.375C11.7538 9.125 11.25 9.62949 11.25 10.2521V14.7479C11.25 15.3702 11.7538 15.875 12.375 15.875H12.9375C14.1803 15.875 15.1875 14.8657 15.1875 13.6208V11.3792C15.1875 10.1343 14.1803 9.125 12.9375 9.125ZM9 0.125C3.96949 0.125 0.160664 4.31316 0 9.125V13.0625C0 13.3733 0.251719 13.625 0.5625 13.625H1.125C1.43578 13.625 1.6875 13.3733 1.6875 13.0625V9.125C1.6875 5.09363 4.96863 1.81953 9 1.81883C13.0314 1.81953 16.3125 5.09363 16.3125 9.125V13.0625C16.3125 13.3733 16.5642 13.625 16.875 13.625H17.4375C17.7483 13.625 18 13.3733 18 13.0625V9.125C17.8393 4.31316 14.0305 0.125 9 0.125Z" fill="white"/>
                  </svg>
                </div>
                <div className="text-white text-sm">
                  <p>290k</p>
                </div>
              </div>
              <div className="border-2 border-solid border-black-50 rounded-full w-24 h-12 flex justify-between items-center">
                <div className="w-1/2 h-full flex justify-center items-center">
                  <svg className="w-4 h-4" viewBox="0 0 21 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.08991 1.96501C3.02755 1.02765 4.29909 0.501077 5.62491 0.501077C6.95073 0.501077 8.22227 1.02765 9.15991 1.96501L10.6249 3.42876L12.0899 1.96501C12.5511 1.48746 13.1029 1.10655 13.7129 0.844502C14.3229 0.582457 14.979 0.444526 15.6429 0.438757C16.3068 0.432988 16.9652 0.559496 17.5797 0.810901C18.1942 1.06231 18.7524 1.43357 19.2219 1.90303C19.6913 2.3725 20.0626 2.93076 20.314 3.54524C20.5654 4.15972 20.6919 4.81812 20.6862 5.48202C20.6804 6.14591 20.5425 6.80201 20.2804 7.41203C20.0184 8.02205 19.6375 8.57378 19.1599 9.03501L10.6249 17.5713L2.08991 9.03501C1.15255 8.09737 0.625977 6.82583 0.625977 5.50001C0.625977 4.17419 1.15255 2.90265 2.08991 1.96501Z" fill="#2BD268"/>
                  </svg>
                </div>
                <div className="cursor-pointer hover:border-green-400 border-2 border-solid flex justify-center items-center border-white rounded-full w-1/2 h-full group">
                  <svg className="w-4 h-4 group-hover:fill-green-400 group-hover:stroke-green-400" width="20" height="24" viewBox="0 0 20 24" fill="white" stroke="white" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.57197 2.49048L16.829 9.33064C17.2684 9.62364 17.6287 10.0238 17.8779 10.4894C18.1271 10.9551 18.2574 11.4751 18.2574 12.0032C18.2574 12.5313 18.1271 13.0513 17.8779 13.5169C17.6287 13.9826 17.2684 14.3795 16.829 14.6725L6.57197 21.5127C6.0888 21.8351 5.52717 22.0202 4.947 22.0484C4.36684 22.0766 3.78991 21.9467 3.27777 21.6727C2.76562 21.3987 2.33747 20.9908 2.039 20.4925C1.74053 19.9942 1.58293 19.4242 1.58302 18.8434V5.16622C1.58176 4.58483 1.73852 4.01403 2.03652 3.51482C2.33453 3.01561 2.76258 2.60676 3.27493 2.33197C3.78729 2.05718 4.36468 1.92677 4.9454 1.95469C5.52612 1.98261 6.08834 2.1678 6.57197 2.49048Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </div>  
              </div>
            </div>
          </div>

          <div className="mb-5 last-of-type:mb-0 listened-tracks-card w-full flex justify-between items-center">
            <div className="flex justify-center items-center">
              <div className="rounded w-12 h-12">
                <img className="rounded" alt="Listened track image" src={require('../../Assets/Images/image2.jpg')}/>
              </div>
              <div className="block ml-4">
                <div className="mb-1 text-sm font-semi-bold text-white font-dm">
                  <h4>Psalm Trees</h4>
                </div>
                <div className="text-sm text-gray-400 font-dm">
                  <h4>Clocks forward</h4>
                </div>
              </div>
            </div>
            <div className="flex justify-center items-center">
              <div className="mr-4 flex justify-center items-center">
                <div className="mr-2 w-4.5 h-3.5">
                  <svg className="w-full h-full" viewBox="0 0 18 16" fill="none"  xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.625 9.125H5.0625C3.81973 9.125 2.8125 10.134 2.8125 11.3792V13.6208C2.8125 14.8657 3.81973 15.875 5.0625 15.875H5.625C6.24621 15.875 6.75 15.3702 6.75 14.7479V10.2521C6.75 9.62949 6.24621 9.125 5.625 9.125ZM12.9375 9.125H12.375C11.7538 9.125 11.25 9.62949 11.25 10.2521V14.7479C11.25 15.3702 11.7538 15.875 12.375 15.875H12.9375C14.1803 15.875 15.1875 14.8657 15.1875 13.6208V11.3792C15.1875 10.1343 14.1803 9.125 12.9375 9.125ZM9 0.125C3.96949 0.125 0.160664 4.31316 0 9.125V13.0625C0 13.3733 0.251719 13.625 0.5625 13.625H1.125C1.43578 13.625 1.6875 13.3733 1.6875 13.0625V9.125C1.6875 5.09363 4.96863 1.81953 9 1.81883C13.0314 1.81953 16.3125 5.09363 16.3125 9.125V13.0625C16.3125 13.3733 16.5642 13.625 16.875 13.625H17.4375C17.7483 13.625 18 13.3733 18 13.0625V9.125C17.8393 4.31316 14.0305 0.125 9 0.125Z" fill="white"/>
                  </svg>
                </div>
                <div className="text-white text-sm">
                  <p>460k</p>
                </div>
              </div>
              <div className="border-2 border-solid border-black-50 rounded-full w-24 h-12 flex justify-between items-center">
                <div className="w-1/2 h-full flex justify-center items-center">
                  <svg className="w-4 h-4" viewBox="0 0 21 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.08991 1.96501C3.02755 1.02765 4.29909 0.501077 5.62491 0.501077C6.95073 0.501077 8.22227 1.02765 9.15991 1.96501L10.6249 3.42876L12.0899 1.96501C12.5511 1.48746 13.1029 1.10655 13.7129 0.844502C14.3229 0.582457 14.979 0.444526 15.6429 0.438757C16.3068 0.432988 16.9652 0.559496 17.5797 0.810901C18.1942 1.06231 18.7524 1.43357 19.2219 1.90303C19.6913 2.3725 20.0626 2.93076 20.314 3.54524C20.5654 4.15972 20.6919 4.81812 20.6862 5.48202C20.6804 6.14591 20.5425 6.80201 20.2804 7.41203C20.0184 8.02205 19.6375 8.57378 19.1599 9.03501L10.6249 17.5713L2.08991 9.03501C1.15255 8.09737 0.625977 6.82583 0.625977 5.50001C0.625977 4.17419 1.15255 2.90265 2.08991 1.96501Z" fill="#2BD268"/>
                  </svg>
                </div>
                <div className="cursor-pointer hover:border-green-400 border-2 border-solid flex justify-center items-center border-white rounded-full w-1/2 h-full group">
                  <svg className="w-4 h-4 group-hover:fill-green-400 group-hover:stroke-green-400" width="20" height="24" viewBox="0 0 20 24" fill="white" stroke="white" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.57197 2.49048L16.829 9.33064C17.2684 9.62364 17.6287 10.0238 17.8779 10.4894C18.1271 10.9551 18.2574 11.4751 18.2574 12.0032C18.2574 12.5313 18.1271 13.0513 17.8779 13.5169C17.6287 13.9826 17.2684 14.3795 16.829 14.6725L6.57197 21.5127C6.0888 21.8351 5.52717 22.0202 4.947 22.0484C4.36684 22.0766 3.78991 21.9467 3.27777 21.6727C2.76562 21.3987 2.33747 20.9908 2.039 20.4925C1.74053 19.9942 1.58293 19.4242 1.58302 18.8434V5.16622C1.58176 4.58483 1.73852 4.01403 2.03652 3.51482C2.33453 3.01561 2.76258 2.60676 3.27493 2.33197C3.78729 2.05718 4.36468 1.92677 4.9454 1.95469C5.52612 1.98261 6.08834 2.1678 6.57197 2.49048Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </div>  
              </div>
            </div>
          </div>

      </div>
        
    </div>
  );
}